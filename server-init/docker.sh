#!/bin/bash

# docker install
is_exist=$(sudo yum list installed docker-ce 2>&1 | grep "No matching Packages" | wc -l)
if [ "${is_exist}" -gt 0 ]; then
 sudo yum install -y yum-utils device-mapper-persistent-data lvm2
 sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
 sudo yum install -y docker-ce
 sudo systemctl enable docker
 sudo systemctl start docker
fi

# docker enter
is_exist=$(cat /root/.bashrc | grep docker-enter | wc -l)
if [ "${is_exist}" -eq 0 ]; then
echo \
'
alias docker-enter="f() { docker exec -it \$1 /bin/bash; }; f"
_docker-enter() {
    local cur containers
    cur=${COMP_WORDS[COMP_CWORD]}
    containers=$(docker container ls --format {{.Names}})
    COMPREPLY=( $( compgen -W "$containers" -- $cur ) )
}
complete -F _docker-enter docker-enter
' >> /root/.bashrc
fi